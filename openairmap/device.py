import os
import threading
import time
import logging
import serial
import pyudev

from openairmap.decorators import log_exception, need_device

RATE = 115200
TIMEOUT = 10
DEVICE_DEFAULT = dict(port=None, version=None, controler=None)


class OamDevice(threading.Thread):
    def __init__(self, *args, event_callback=lambda: None, **kwargs):
        super().__init__(*args, **kwargs)
        self.event_callback = event_callback
        self.shutdown_request = False
        self.on_request = False
        self.serial = None
        self.device = DEVICE_DEFAULT.copy()
        self.observer = None
        self.request_map = list()
        self.start()

    @log_exception
    def run(self):
        # Set up udev observer
        context = pyudev.Context()
        monitor = pyudev.Monitor.from_netlink(context)
        self.observer = pyudev.MonitorObserver(monitor, self._usb_monitor_cb)
        self.observer.start()
        # Search for connected devices
        for device in context.list_devices():
            self._usb_monitor_cb('add', device)
        # Read loop
        while not self.shutdown_request:
            time.sleep(.5)
            if not self.serial:
                continue
            if not self.on_request:
                if self.request_map:
                    self._send(self.request_map.pop(0))
                continue
            rawdata = self.serial.readline().decode('utf-8').strip()
            self.on_request = False
            if rawdata:
                self.event_callback('receive', rawdata)
        self.disconnect()

    @log_exception
    def _connect(self, port):
        self.serial = serial.Serial(f'/dev/{port}', RATE, timeout=TIMEOUT)
        self.serial.rts = False
        logging.info('Connected to /dev/%s', port)
        return self.serial

    def disconnect(self):
        if self.serial:
            logging.info('Disconnecting device')
            self.serial.close()
            self.serial = None
        self.device = DEVICE_DEFAULT.copy()
        self.request_map = list()
        self.event_callback('remove')
        return self.serial

    def shutdown(self):
        self.disconnect()
        self.observer.stop()
        self.shutdown_request = True

    @need_device
    @log_exception
    def _send(self, request):
        self.on_request = True
        logging.debug('Sending request %s to device %s', request, self.device)
        self.serial.write(f'{request}\r'.encode('utf-8'))

    @need_device
    @log_exception
    def _identify(self):
        self.serial.write(b'ID\r')
        response = self.serial.readline().decode('utf-8').strip().split('|')
        logging.debug('Identify response: %s', response)
        if not response or len(response) < 3:
            return self.disconnect()
        # if 'OAMDEVICE' not in response[0][3:]:
        #     return self.disconnect()
        self.device['version'] = response[1]
        self.device['controler'] = response[2]
        return self.serial

    def _usb_monitor_cb(self, action, device):
        if device.subsystem != 'tty' or 'usb' not in device.sys_path.lower():
            return
        if action == 'add':
            port = os.path.basename(device.sys_path)
            time.sleep(1)
            if not self._connect(port):
                return
            time.sleep(1)
            if not self._identify():
                return
            self.device['port'] = port
            self.event_callback(action, self.device)
            self.fetch_config()
            self.fetch_data()
        elif action == 'remove':
            self.disconnect()
            # self.request_map = list()
            # self.event_callback(action)

    def fetch_config(self):
        self.request_map.append('CF')

    def fetch_data(self):
        self.request_map.append('FT')

    def clear_data(self):
        self.request_map.append('CR')

    def set_config(self, config):
        sleepmode = str(config['sleepmode'])
        datafreq = str(config['datafreq'])
        datafreq = f'0{datafreq}' if len(datafreq) == 1 else datafreq
        gpstimeout = str(config['gpstimeout'])
        gpstimeout = f'0{gpstimeout}' if len(gpstimeout) == 1 else gpstimeout
        self.request_map.append(f'CF:{sleepmode}{datafreq}{gpstimeout}')

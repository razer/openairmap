import logging
import json

import gi
gi.require_version('Gtk', '3.0')
# pylint: disable=C0413
from gi.repository import Gtk

from openairmap.renderers import render_datetime, render_float, render_status

class Treeview:
    def __init__(self, builder, ws_server, row_deleted_cb):
        self.widget = builder.get_object
        self.ws_server = ws_server
        self.row_deleted_cb = row_deleted_cb
        self.trv = self.widget('trv_data')
        self.tms = self.widget('tms_data')
        self.deleted = list()
        col_status = self.trv.get_column(0)
        rend = self.widget('rdr_status')
        col_status.set_cell_data_func(rend, render_status)
        col_time = self.trv.get_column(1)
        rend = self.widget('rdr_time')
        col_time.set_cell_data_func(rend, render_datetime)
        for index in range(2, 6):
            col = self.trv.get_column(index)
            rend = self.widget(f'rdr_col{index}')
            col.set_cell_data_func(rend, render_float)
        self.tms.set_sort_column_id(1, Gtk.SortType.DESCENDING)
        # Bypass selection change on right click
        self.right_click = False
        selection = self.trv.get_selection()
        selection.set_select_function(self.bypass_right_click)

    def on_selection_changed(self, treeselection):
        model, path_map = treeselection.get_selected_rows()
        if not path_map:
            return
        selected = model.get_value(model.get_iter(path_map[0]), 0)
        if selected.have_gps:
            self.ws_server.send(json.dumps({'selected': selected.as_dict}))

    def on_row_clicked(self, unused_treeview, event):
        self.right_click = event.button == 3
        if event.button == 3:
            self.widget('data_menu').popup_at_pointer()

    def on_row_released(self, unused_treeview, unused_event):
        self.right_click = False

    def on_row_deleted(self, unused_menuitem):
        selection = self.trv.get_selection()
        model, path_map = selection.get_selected_rows()
        if not path_map:
            return
        self.right_click = False
        selection.unselect_all()
        self.deleted = list()
        for path in reversed(path_map):
            tms_iter = model.get_iter(path)
            self.deleted.append(model.get_value(tms_iter, 0))
            lst_iter = model.convert_iter_to_child_iter(tms_iter)
            model.get_model().remove(lst_iter)
        self.row_deleted_cb()

    def on_select_all(self, unused_widget):
        self.right_click = False
        self.trv.get_selection().select_all()

    def on_unselect_all(self, unused_widget):
        self.right_click = False
        self.trv.get_selection().unselect_all()

    def bypass_right_click(self, selection, unused_1, unused_2, unused_3):
        return None if self.right_click else selection

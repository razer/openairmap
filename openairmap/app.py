#!bin/env python3
import os
import sys
import json
import random
import csv
from datetime import datetime, timedelta
import time
import logging
import asyncio
import threading
import pickle

import gi
gi.require_version('Gtk', '3.0')
gi.require_version("WebKit2", "4.0")
# pylint: disable=C0413
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk, Gtk, WebKit2, GdkPixbuf

# For direct run without installation (direct file execution)
DEVMODE = False
if __name__ == '__main__':
    sys.path.insert(0, os.getcwd())
    DEVMODE = True

from openairmap.utils import _
from openairmap.config import Config as C, PACKAGE_NAME, CONFIG_DIR, DATE_FORMAT
from openairmap.utils import get_file, get_realpath
from openairmap.decorators import thread_safe, check_ready
from openairmap.data import OamData
from openairmap.wsserver import WsServer
from openairmap.device import OamDevice
from openairmap.treeview import Treeview

class OamApp:  # pylint: disable=R0904
    def __init__(self):
        self.ready = False
        self.state_id = None
        self.on_action = None
        self.ws_server = WsServer(asyncio.get_event_loop())
        self.ws_server.start()
        self.device = OamDevice(event_callback=self.on_device_event)
        self.cfg = C(devmode=DEVMODE)
        self.data = dict(file=os.path.join(CONFIG_DIR, 'data.pickle'))
        self.data['deleted'] = os.path.join(CONFIG_DIR, 'deleted.pickle')
        self.data['map'] = self.saved_data_map
        self.data['filter'] = self.data['map']
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_file(f'{PACKAGE_NAME}.ui'))
        self.treeview = Treeview(builder, self.ws_server, self.on_data_deleted)
        builder.connect_signals({
            'on_window_resize': self.on_window_resize,
            'on_paned_resized': self.on_paned_resized,
            'on_rev_btn_clicked': self.on_rev_btn_clicked,
            'on_force_action_clicked' : self.on_force_action_clicked,
            'on_appsettings_clicked': self.on_appsettings_clicked,
            'on_appsettings_cancel': self.on_appsettings_cancel,
            'on_appsettings_ok': self.on_appsettings_ok,
            'on_devsettings_clicked': self.on_devsettings_clicked,
            'on_devsettings_cancel': self.on_devsettings_cancel,
            'on_devsettings_ok': self.on_devsettings_ok,
            'on_devdisconnect_clicked' : self.on_devdisconnect_clicked,
            'on_devclear_clicked' : self.on_devclear_clicked,
            'on_devcheck_clicked' : self.on_devcheck_clicked,
            'on_selection_changed': self.treeview.on_selection_changed,
            'on_row_clicked': self.treeview.on_row_clicked,
            'on_row_released': self.treeview.on_row_released,
            'on_row_deleted': self.treeview.on_row_deleted,
            'on_select_all': self.treeview.on_select_all,
            'on_unselect_all': self.treeview.on_unselect_all,
            'on_today_clicked': lambda w: self.filter_data(w, 'today'),
            'on_3days_clicked': lambda w: self.filter_data(w, '3days'),
            'on_week_clicked': lambda w: self.filter_data(w, 'week'),
            'on_month_clicked': lambda w: self.filter_data(w, 'month'),
            'on_anytime_clicked': lambda w: self.filter_data(w, 'anytime'),
            'on_csv_export': self.on_csv_export,
            'on_csv_ok': self.on_csv_ok,
            'on_csv_cancel': self.on_csv_cancel,
            'on_dia_confim_ok': lambda w: self.on_dia_confim(True),
            'on_dia_confim_cancel': lambda w: self.on_dia_confim(False),
            'on_about_clicked': self.show_about,
        })
        self.widget = builder.get_object
        self.refresh_uidata()
        # About Logo
        pbuf = GdkPixbuf.Pixbuf.new_from_file(get_file(f'{PACKAGE_NAME}.png'))
        self.widget('dia_about').set_logo(pbuf)
        # Webkit view
        webview = WebKit2.WebView()
        webview.set_visible(True)
        webview.connect('context-menu', self.webkit_menu_bypass)
        wv_settings = webview.get_settings()
        wv_settings.set_enable_write_console_messages_to_stdout(True)
        path = get_realpath()
        if path:
            webview.load_uri(f'file://{path}/osm/index.html')
        provider = Gtk.CssProvider()
        provider.load_from_path(get_file(f'{PACKAGE_NAME}.css'))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.widget('scr_webkit').add(webview)
        # Window init
        window = self.widget('main_window')
        # self.widget('main_title').set_text(_('OpenAirMap - no device'))
        window.connect('destroy', self.quit)
        window.resize(self.cfg.get(C.IFACE_SECTION, C.WIN_WIDTH),
                      self.cfg.get(C.IFACE_SECTION, C.WIN_HEIGHT))
        self.widget('pnd_main').set_position(
            self.cfg.get(C.IFACE_SECTION, C.PANE_POS))
        window.show()
        self.ready = True
        Gtk.main()

    @classmethod
    def webkit_menu_bypass(cls, unused_1, unused_2, unused_3, unused_4):
        return True

    @property
    def deleted_hash_map(self):
        if not os.path.isfile(self.data['deleted']):
            return []
        with open(self.data['deleted'], 'rb') as hash_file:
            try:
                return list(pickle.load(hash_file))
            except Exception:
                logging.exception('', exc_info=True)
        return []

    @property
    def saved_data_map(self):
        if not os.path.isfile(self.data['file']):
            return []
        with open(self.data['file'], 'rb') as data_file:
            try:
                return list(map(
                    lambda d: OamData(self.cfg[C.PMLVL_SECTION], d),
                    pickle.load(data_file)))
            except Exception:
                logging.exception('', exc_info=True)
        return []

    @thread_safe
    @check_ready
    def on_device_event(self, action, rawdata=None):
        # logging.info('Got %s from device', rawdata)
        if action in ('add', 'remove'):
            self.widget('mbt_device').set_visible(action == 'add')
            self.widget('main_title').set_visible(action == 'remove')
            if action == 'add' and rawdata:
                self.widget('dev_lbl').set_text(
                    _('OpenAirMap device ')
                    + f' - {rawdata["controler"]} ({rawdata["version"]})')
            return

        if action != 'receive' or not rawdata or len(rawdata) < 3:
            logging.warning('Got bad response from device: %s', rawdata)
            return
        # Response from device
        data_identifier = rawdata[:3]
        rawdata = rawdata[3:]
        if data_identifier == 'FT:':
            # Data fetch from device
            pm_levels = self.cfg[C.PMLVL_SECTION]
            update = False
            previously_deleted_hash_map = self.deleted_hash_map
            for new_data in map(lambda nd: OamData.from_raw(pm_levels, nd),
                                rawdata.split('&')):
                if not new_data:
                    continue
                if new_data.hash in previously_deleted_hash_map:
                    logging.debug('Data with hash %s was previously deleted',
                                  new_data.hash)
                    continue
                if new_data not in self.data['map']:
                    update = True
                    self.data['map'].append(new_data)
            if update:
                self.widget('lst_data').clear()
                self.refresh_uidata()
        elif data_identifier == 'CF:':
            # Device config data
            if 'OK' in rawdata:
                self.widget('win_devsettings').hide()
                return
            if len(rawdata) < 5:
                logging.error('Bad config data: %s', rawdata)
                return
            self.set_selected(int(rawdata[0]), 'lst_devsleep', 'cmb_sleepmode')
            self.set_selected(int(rawdata[1:3]), 'lst_datafreq', 'cmb_datafreq')
            self.set_selected(int(rawdata[3:5]), 'lst_gpstimeout',
                              'cmb_gpstimeout')

    def set_selected(self, value, list_name, widget_name):
        active = list(filter(lambda t: t[0] == value, self.widget(list_name)))
        if not active:
            return
        self.widget(widget_name).set_active_iter(active[0].iter)

    def get_selected_value(self, widget_name):
        value = self.widget(widget_name).get_active_iter()
        if not value:
            return None
        return self.widget(widget_name).get_model()[value][0]

    def refresh_uidata(self):
        self.refresh_osm_data()
        for data in self.data['filter']:
            self.widget('lst_data').append([data,] + data)

    def refresh_osm_data(self, gps_only=True):
        if gps_only:
            osm_data_map = filter(lambda nd: nd.have_gps, self.data['filter'])
        else:
            osm_data_map = self.data['filter'].copy()
        if self.treeview.deleted:
            osm_data_map = filter(lambda nd: nd not in self.treeview.deleted,
                                  osm_data_map)
        osm_data_map = list(map(lambda nd: nd.as_dict, osm_data_map))
        self.ws_server.send(json.dumps({
            'zoom': self.cfg.get(C.IFACE_SECTION, C.DEFAULT_ZOOM),
            'map': list(osm_data_map)}))

    def filter_data(self, widget, period):
        now = datetime.now()
        self.widget('lbl_datafilter').set_text(widget.get_property('text'))
        filter_start = None
        if period == 'today':
            filter_start = now - timedelta(hours=now.hour, minutes=now.minute)
        if period == '3days':
            filter_start = now - timedelta(days=3)
        if period == 'week':
            filter_start = now - timedelta(days=7)
        if period == 'month':
            filter_start = now - timedelta(days=30)
        if filter_start:
            self.data['filter'] = list(filter(
                lambda d: datetime.fromtimestamp(d[0]) > filter_start,
                self.data['map']))
        else:
            self.data['filter'] = self.data['map']
        self.widget('lst_data').clear()
        self.refresh_uidata()

    def on_rev_btn_clicked(self, unused_button):
        if self.on_action == 'data_delete':
            for data in self.treeview.deleted:
                self.widget('lst_data').append([data,] + data)
            self.treeview.deleted = list()
            self.refresh_osm_data()
            self.data_delete_sync()
        elif self.on_action == 'dev_data_delete':
            self.delete_dev_data(self.state_id, cancel=True)

    def on_force_action_clicked(self, unused_button):
        if self.on_action == 'dev_data_delete':
            self.delete_dev_data(self.state_id)

    def on_devsettings_clicked(self, unused_mbt):
        self.widget('win_devsettings').show()

    def on_devsettings_ok(self, unused_btn):
        self.device.set_config(dict(
            sleepmode=self.get_selected_value('cmb_sleepmode'),
            datafreq=self.get_selected_value('cmb_datafreq'),
            gpstimeout=self.get_selected_value('cmb_gpstimeout')))

    def on_devsettings_cancel(self, unused_btn):
        self.widget('win_devsettings').hide()

    def on_devdisconnect_clicked(self, unused_btn):
        self.device.disconnect()

    def on_devclear_clicked(self, unused_btn):
        self.on_action = 'dev_data_delete'
        self.widget('box_title').hide()
        self.widget('frc_btn').set_visible(True)
        self.widget('rev_label').set_text(_(
            'Device data will be deleted in a few seconds'))
        self.widget('box_action').show()
        state_id = int(random.random()*100000)
        self.state_id = state_id
        threading.Thread(target=lambda: (
            time.sleep(8), self.delete_dev_data(state_id))).start()

    def on_devcheck_clicked(self, unused_btn):
        self.device.fetch_data()

    def on_appsettings_clicked(self, unused_mbt):
        self.widget('sbt_defzoom').set_value(self.cfg.get(C.IFACE_SECTION,
                                                          C.DEFAULT_ZOOM))
        self.set_selected(self.cfg.get(C.PMLVL_SECTION, C.PMLVL_DEF),
                          'lst_leveldef', 'cmb_pmlvldef')
        self.widget('sbt_pmlvlwarn').set_value(self.cfg.get(C.PMLVL_SECTION,
                                                            C.PMLVL_WARN))
        self.widget('sbt_pmlvldang').set_value(self.cfg.get(C.PMLVL_SECTION,
                                                            C.PMLVL_DANG))
        self.widget('win_appsettings').show()

    def on_appsettings_cancel(self, unused_btn):
        self.widget('win_appsettings').hide()

    def on_appsettings_ok(self, unused_btn):
        self.cfg.set(C.IFACE_SECTION, C.DEFAULT_ZOOM,
                     int(self.widget('sbt_defzoom').get_value()))
        self.cfg.set(C.PMLVL_SECTION, C.PMLVL_DEF,
                     self.get_selected_value('cmb_pmlvldef'))
        self.cfg.set(C.PMLVL_SECTION, C.PMLVL_WARN,
                     int(self.widget('sbt_pmlvlwarn').get_value()))
        self.cfg.set(C.PMLVL_SECTION, C.PMLVL_DANG,
                     int(self.widget('sbt_pmlvldang').get_value()))
        self.widget('win_appsettings').hide()
        self.widget('lst_data').clear()
        self.refresh_uidata()

    def on_csv_export(self, unused_widget):
        self.widget('fcd_csvexport').show()

    def on_csv_cancel(self, unused_widget):
        self.widget('fcd_csvexport').hide()

    def on_csv_ok(self, unused_widget):
        self.widget('fcd_csvexport').hide()
        csv_filename = self.widget('fc_csv').get_filename()
        if csv_filename[-4:].lower() != '.csv':
            csv_filename += '.csv'
        logging.info('Csv file: %s', csv_filename)
        if os.path.exists(csv_filename):
            self.widget('lbl_diaconfirm').set_text(
                os.path.basename(csv_filename) + _(' exist, overwrite it ?'))
            if not self.widget('dia_confirm').run():
                return
        with open(csv_filename, 'w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            csv_writer.writerow(
                ['timestamp', 'datetime', 'tag', 'pm25', 'pm10', 'lat', 'lng'])
            for data in self.data['filter']:
                date = datetime.fromtimestamp(data[0]).strftime(DATE_FORMAT)
                csv_writer.writerow([data[0], date, data.tag] + data[1:])

    def on_dia_confim(self, response):
        self.widget('dia_confirm').response(response)
        self.widget('dia_confirm').hide()

    def on_paned_resized(self, paned, unused_position):
        self.cfg.set(C.IFACE_SECTION, C.PANE_POS, paned.get_position())

    def on_data_deleted(self):
        self.on_action = 'data_delete'
        self.refresh_osm_data()
        self.widget('box_title').hide()
        self.widget('rev_label').set_text(
            _('Removing ') + str(len(self.treeview.deleted))
            + _(' datas from list'))
        self.widget('box_action').show()
        threading.Thread(
            target=lambda: (time.sleep(8), self.data_delete_sync())).start()

    @thread_safe
    def delete_dev_data(self, state_id, cancel=False):
        if self.on_action != 'dev_data_delete' or state_id != self.state_id:
            return
        self.widget('frc_btn').set_visible(False)
        self.widget('box_action').hide()
        self.widget('box_title').show()
        self.on_action = self.state_id = None
        if cancel:
            return
        logging.info('Deleting device data')
        self.device.clear_data()

    @thread_safe
    def data_delete_sync(self):
        if self.on_action != 'data_delete':
            return
        self.on_action = None
        for data in self.treeview.deleted:
            self.data['map'].remove(data)
        hash_map = list(map(lambda d: d.hash, self.treeview.deleted))  # pylint: disable=W0108
        hash_map += self.deleted_hash_map
        with open(self.data['deleted'], 'wb') as hash_file:
            pickle.dump(hash_map, hash_file, pickle.HIGHEST_PROTOCOL)
        self.treeview.deleted = list()
        self.widget('box_action').hide()
        self.widget('box_title').show()

    def on_window_resize(self, window, unused_thing):
        width, height = window.get_size()
        self.cfg.set(C.IFACE_SECTION, C.WIN_WIDTH, width)
        self.cfg.set(C.IFACE_SECTION, C.WIN_HEIGHT, height)

    def show_about(self, unused_widget):
        self.widget('dia_about').show()

    def quit(self, unused_widget):
        self.device.shutdown()
        with open(self.data['file'], 'wb') as data_file:
            saved_data_map = map(lambda nd: list(nd), self.data['map'])  # pylint: disable=W0108
            pickle.dump(list(saved_data_map), data_file,
                        pickle.HIGHEST_PROTOCOL)
        self.ws_server.shutdown()
        self.cfg.save()
        Gtk.main_quit()


if __name__ == '__main__':
    OamApp()

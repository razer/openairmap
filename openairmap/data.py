from datetime import datetime, timedelta
import json

from openairmap.config import DATE_FORMAT

class OamData(list):
    def __init__(self, pm_levels, *args, **kwargs):
        self.pm_levels = pm_levels
        super().__init__(*args, **kwargs)

    @classmethod
    def from_raw(cls, pm_levels, rawdata):
        rawdata = rawdata.split('|')
        if len(rawdata) < 5:
            return None
        data_list = list()
        data_list.append(int(rawdata.pop(0)))
        for data in rawdata:
            data_list.append(float(data))
        return OamData(pm_levels, data_list)

    def __repr__(self):
        return (
            f'{self.human_date}: '
            + f'pm25 {round(self[1], 1)} - pm10 {round(self[2], 1)}'
            + f' - lat {round(self[3], 4)} - lng {round(self[4], 4)}')

    @property
    def hash(self):
        return tuple(self).__hash__()

    @property
    def as_dict(self):
        return dict(date=self.human_date, pm25=round(self[1], 1),
                    pm10=round(self[2], 1), latitude=self[3], longitude=self[4],
                    tag=self.tag, hash=tuple(self).__hash__())

    @property
    def tag(self):
        for tag in 'danger', 'warning':
            if self[1]+self[2] > float(self.pm_levels[tag]):
                return tag
        return 'default'

    @property
    def serialized(self):
        return json.dumps(self.as_dict)

    @property
    def have_gps(self):
        return bool(self[3] or self[4])

    @property
    def coordonates(self):
        if not self[3] and not self[4]:
            return None
        return self[3:5]

    @property
    def human_date(self):
        input_dt = datetime.fromtimestamp(self[0])
        now = datetime.now()
        today_start = now - timedelta(hours=now.hour, minutes=now.minute)
        if now.day == input_dt.day:
            return f'Today {input_dt.strftime("%H:%M")}'
        if today_start - input_dt < timedelta(days=1):
            return f'Yesterday {input_dt.strftime("%H:%M")}'
        if datetime.now() - input_dt < timedelta(days=7):
            return input_dt.strftime("%a %H:%M")
        return input_dt.strftime(DATE_FORMAT)

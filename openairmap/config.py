import os
import sys
import logging
from logging.handlers import RotatingFileHandler
from configparser import RawConfigParser, Error as ConfigParserError
import xdg.BaseDirectory as bd

APP_VERSION = '0.99'
PACKAGE_NAME = 'openairmap'
# LOGLEVEL = logging.DEBUG
WSLOGLEVEL = logging.INFO
DATE_FORMAT = '%d/%m/%y %H:%M'

CONFIG_DIR = os.path.join(bd.xdg_config_home, PACKAGE_NAME)
LOCALE_DIR = '/usr/share/locale'

def log_init(devmode=False, debug=False):
    logfmt = '%(asctime)s %(levelname)s:%(module)s(%(lineno)s): %(message)s'
    datefmt = '%d/%m %H:%M:%S'
    if devmode:
        print('Development mode detected : setting log to stdout')
        logging.basicConfig(format=logfmt, datefmt=datefmt, level=logging.DEBUG,
                            stream=sys.stdout)
    else:
        logging_filehandler = RotatingFileHandler(
            os.path.join(CONFIG_DIR, f'{PACKAGE_NAME}.log'), mode='a',
            maxBytes=1000000, backupCount=7, encoding='utf-8', delay=0)
        logging_filehandler.setLevel(logging.DEBUG if debug else logging.INFO)
        logging.basicConfig(format=logfmt, datefmt=datefmt, level=logging.DEBUG,
                            handlers=[logging_filehandler])
    logging.getLogger('websockets').setLevel(WSLOGLEVEL)


class Config(RawConfigParser):
    CORE_SECTION = 'core'
    IFACE_SECTION = 'interface'
    PMLVL_SECTION = 'pm_levels'
    DEBUG = 'debug'
    WIN_WIDTH = 'window_width'
    WIN_HEIGHT = 'window_height'
    PANE_POS = 'paned_position'
    DEFAULT_ZOOM = 'default_zoom'
    PMLVL_DEF = 'pmlevels_def'
    PMLVL_WARN = 'warning'
    PMLVL_DANG = 'danger'
    CORE_DEFAULTS = {DEBUG:0}
    PMLVL_DEFAULTS = {PMLVL_DEF: 'cumulate', PMLVL_WARN:20, PMLVL_DANG:40}
    IFACE_DEFAULTS = {WIN_WIDTH:800, WIN_HEIGHT:600, PANE_POS:500,
                      DEFAULT_ZOOM: 10}
    SECTIONS = (CORE_SECTION, IFACE_SECTION, PMLVL_SECTION)
    SECTIONS_DEFAULTS = (CORE_DEFAULTS, IFACE_DEFAULTS, PMLVL_DEFAULTS)

    def __init__(self, *args, load=True, devmode=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._file = os.path.join(CONFIG_DIR, f'{PACKAGE_NAME}.cfg')
        if load:
            self.load(devmode=devmode)

    def __repr__(self):
        return str({s: dict(self[s]) for s in self.sections()})

    def _load_defaults(self, error_msg=None, warn_msg=None):
        if error_msg:
            logging.error(error_msg)
        if warn_msg:
            logging.warning(warn_msg)
        for section, def_values in zip(self.SECTIONS, self.SECTIONS_DEFAULTS):
            self[section] = def_values

    def _load_section(self, section, defaults):
        for config_key, default_value in defaults.items():
            if config_key not in self[section]:
                logging.info('Adding %s->%s', config_key, default_value)
                self.set(section, config_key, default_value)

    def get(self, section, option, *args, **kwargs):  # pylint: disable=W0221
        value = super().get(section, option, *args, **kwargs)
        try:
            return int(value)
        except ValueError:
            return value

    def load(self, devmode=False):
        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)
            self._load_defaults()
            return
        if not os.path.exists(self._file):
            self._load_defaults(warn_msg='No config file found')
            return
        try:
            self.read(self._file)
        except ConfigParserError:
            self._load_defaults(error_msg='Config file is corrupted')
            return
        for section, def_values in zip(self.SECTIONS, self.SECTIONS_DEFAULTS):
            if section not in self.sections():
                self._load_defaults(error_msg=f'section "{section}" not found')
                return
            self._load_section(section, def_values)
        # Prepare logging
        log_init(devmode)
        logging.debug('Config loaded: %s', self)

    def save(self):
        logging.debug('Saving config: %s', self)
        with open(self._file, 'w') as configfile:
            self.write(configfile)

## Open Air Map
![Logo](https://framagit.org/razer/openairmap/raw/master/artwork/oam-icon.svg)

![Screenshot](https://framagit.org/razer/openairmap/raw/master/artwork/oam-shot-main.png)

## In developpement - Use at your own risks

OpenAirMap is a GTK application providing a map with pollution data (actually pm2.5 and pm10 particules) coming from an ESP16/32 mobile device with NovaPm (or other dust sensor) and a GPS.

The device firmware have his [own repo](https://framagit.org/razer/openairmap_device)
